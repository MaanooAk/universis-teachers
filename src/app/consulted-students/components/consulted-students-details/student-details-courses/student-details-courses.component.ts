import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService } from '@universis/common';
import { LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { ResponseError } from '@themost/client';
import { BasicLoadComponent } from 'src/app/generics/components/BasicLoadComponent';


@Component({
  selector: 'app-student-details-courses',
  templateUrl: './student-details-courses.component.html',
  styleUrls: []
})
export class StudentDetailsCoursesComponent extends BasicLoadComponent {

  public list = [];

  async load({ studentId }) {

    const student = await this.context.model('Instructors/me/consultedStudents')
      .where('student').equal(studentId)
      .expand('student($expand=studentStatus,department,studyProgram,inscriptionPeriod)')
      .getItem()

    if (!student) throw new ResponseError('Student not found', 404);

    const list = await this.context
      .model(`Instructors/me/consultedStudents/${studentId}/Courses`).asQueryable()
      .expand(`programGroup($expand=groupType), studyProgramSpecialty($expand=locale), course($expand=instructor,gradeScale,courseCategory,courseSector,courseArea), gradeExam($expand=examPeriod,year,instructors($expand=instructor))`)
      .orderBy('course/displayCode')
      .take(-1)
      .getItems();

    list.forEach(item => Object.assign(item, {
      tableConfig: [
        ['Students.CourseDisplayCode', item.course.displayCode],
        ['Students.Course', item.courseTitle],
        ['Students.Type', item.courseType.name],
        ['StudentCourses.courseCategory', item.course.courseCategory?.alternateName ?? '-'],
        ['Students.Grade', item.formattedGrade ?? '-'],
      ]
    }))

    this.list = list;
  }

}
