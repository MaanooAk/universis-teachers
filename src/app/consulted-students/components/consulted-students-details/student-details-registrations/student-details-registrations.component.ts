import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService } from '@universis/common';
import { LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { ResponseError } from '@themost/client';
import { BasicLoadComponent } from 'src/app/generics/components/BasicLoadComponent';


@Component({
  selector: 'app-student-details-registrations',
  templateUrl: './student-details-registrations.component.html',
  styleUrls: []
})
export class StudentDetailsRegistrationsComponent extends BasicLoadComponent {

  public list = [];

  async load({ studentId }) {

    const student = await this.context.model('Instructors/me/consultedStudents')
      .where('student').equal(studentId)
      .expand('student($expand=studentStatus,department,studyProgram,inscriptionPeriod)')
      .getItem()

    if (!student) throw new ResponseError('Student not found', 404);

    const list = await this.context
      .model(`Instructors/me/consultedStudents/${studentId}/Registrations`).asQueryable()
      .select("id",
        "registrationYear/alternateName as academicYear",
        "registrationPeriod/name as period",
        "semester",
        "registrationDate",
        // "status/alternateName as status",
        "registrationType/name as registrationType"
      )
      .orderByDescending('registrationYear')
      .thenByDescending('registrationPeriod')
      .take(-1)
      .getItems();

    list.forEach(item => Object.assign(item, {
      itemConfig: {
        header: [
          ['Labels.AcademicYear', item.academicYear],
          ['Labels.PeriodLabel', item.period]
        ],
        body: [
          ['Students.semester', item.semester],
          ['Registrations.ListTable.RegistrationDate', item.registrationDate.toLocaleDateString(), 'fa fa-calendar'],
          ['Registrations.PeriodRegistrationType', item.registrationType]
        ]
      },
      tableConfig: [
        ['Labels.AcademicYear', item.academicYear],
        ['Labels.PeriodLabel', item.period],
        ['Students.semester', item.semester],
        ['Registrations.ListTable.RegistrationDate', item.registrationDate.toLocaleDateString()],
        ['Registrations.PeriodRegistrationType', item.registrationType]
      ]
    }))

    this.list = list;
  }

}
