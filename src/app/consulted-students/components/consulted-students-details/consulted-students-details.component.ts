import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService } from '@universis/common';
import { ResponseError } from '@themost/client';

@Component({
  selector: 'app-consulted-students-details',
  templateUrl: './consulted-students-details.component.html',
  styleUrls: []
})
export class ConsultedStudentsDetailsComponent implements OnInit {

  public consultedStudent: any;
  public modelTabs: boolean;

  public loading = true;

  constructor(private errorService: ErrorService,
    private _context: AngularDataContext,
    private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.route.params.subscribe(async routeParams => {
      try {
        const studentId = routeParams.studentId;
        const result = await this._context.model('Instructors/me/consultedStudents')
          .where('student').equal(studentId)
          .expand('student')
          .getItem()
        if (typeof result === "undefined") {
          throw new ResponseError('Student not found', 404);
        }
        this.consultedStudent = result;

        const accessibleModels = await this._context.model('Instructors/me/consultedStudents/' + studentId + '/accessibleModels')
          .asQueryable().getItem()
        this.modelTabs = accessibleModels?.length > 0;

        this.loading = false;
      } catch (err) {
        this.loading = false;
        return this.errorService.navigateToError(err);
      }
    });
  }

}
