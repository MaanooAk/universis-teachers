import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultedStudentsTableComponent } from './components/consulted-students-table/consulted-students-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { TablesModule } from '@universis/ngx-tables';
import { ConsultedStudentsRoutingModule } from './consulted-students-routing.module';
import { ConsultedStudentsHomeComponent } from './components/consulted-students-home/consulted-students-home.component';
// tslint:disable-next-line:max-line-length
import { ConsultedStudentsDefaultTableConfigurationResolver, ConsultedStudentsTableConfigurationResolver, ConsultedStudentsTableSearchResolver } from './components/consulted-students-table/consulted-students-table-config.resolver';
import { ConsultedStudentsSharedModule } from './consulted-students.shared';
import { MessagesSharedModule } from '../messages/messages.shared';
import { GradePipe, SharedModule } from '@universis/common';
import {RouterModalModule} from '@universis/common/routing';
import { ConsultedStudentsDetailsComponent } from './components/consulted-students-details/consulted-students-details.component';
import { GradesService } from './services/grades-service/grades-service';
import { FormsModule } from '@angular/forms';
import { StudentDetailsGeneralComponent } from './components/consulted-students-details/student-details-general/student-details-general.component';
import { StudentDetailsCoursesComponent } from './components/consulted-students-details/student-details-courses/student-details-courses.component';
import { StudentDetailsRegistrationsComponent } from './components/consulted-students-details/student-details-registrations/student-details-registrations.component';
import { GenericsSharedModule } from '../generics/generics-shared.module';

// LOCALES: import extra locales here
import * as el from './i18n/consulted-students.el.json';
import * as en from './i18n/consulted-students.en.json';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        TranslateModule,
        ConsultedStudentsSharedModule,
        TablesModule,
        ConsultedStudentsRoutingModule,
        RouterModalModule,
        GenericsSharedModule
    ],
  declarations: [
    ConsultedStudentsHomeComponent,
    ConsultedStudentsTableComponent,
    ConsultedStudentsDetailsComponent,
    StudentDetailsGeneralComponent,
    StudentDetailsCoursesComponent,
    StudentDetailsRegistrationsComponent
  ],
  entryComponents: [
  ],
  providers: [
    ConsultedStudentsTableConfigurationResolver,
    ConsultedStudentsDefaultTableConfigurationResolver,
    ConsultedStudentsTableSearchResolver,
    GradesService,
    GradePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsultedStudentsModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}

