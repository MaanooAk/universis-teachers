import { Component, Input, OnInit, OnDestroy, TemplateRef, ContentChild, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-generic-info',
    templateUrl: './generic-info.component.html',
    styleUrls: ['./generic-info.component.scss']
  })
  export class GenericInfoComponent implements OnInit, OnDestroy {
  
    @Input() public fields: any[];
    
    @Input() public title: any;
    @Input() public classes: any;
  
    // TODO support option to be expandable
    // public expanded = false;

    constructor() {
    }
  
    ngOnInit() {
    }
  
    ngOnDestroy(): void {
    }
  
    ngOnChanges(changes: SimpleChanges) {
    }
  
  
  }
  