import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@universis/common';

import { GenericListComponent } from './components/generic-list/generic-list.component';
import { GenericItemComponent } from './components/generic-item/generic-item.component';
import { GenericInfoComponent } from './components/generic-info/generic-info.component';
import { GenericSwitchComponent } from './components/generic-switch/generic-switch.component';

// LOCALES: import extra locales here
import * as el from './i18n/generics.el.json';
import * as en from './i18n/generics.en.json';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [
    GenericListComponent,
    GenericItemComponent,
    GenericInfoComponent,
    GenericSwitchComponent
  ],
  exports: [
    GenericListComponent,
    GenericItemComponent,
    GenericInfoComponent,
    GenericSwitchComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenericsSharedModule {

  constructor(
    @Optional() @SkipSelf() parentModule: GenericsSharedModule,
    private _translateService: TranslateService
  ) {
    if (parentModule) return;
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
