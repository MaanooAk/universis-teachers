import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {LogoutComponent} from '@universis/common';
import {ErrorBaseComponent, HttpErrorComponent} from '@universis/common';
import {AuthGuard} from '@universis/common';
import {LangComponent} from './teachers-shared/lang-switch.component';
import {ProfileModule} from './profile/profile.module';
import { InstructorOutlineTabsComponent } from '@universis/ngx-qa';
import { InstructorOutlineGeneralComponent } from '@universis/ngx-qa';
import { InstructorOutlinePublicationsComponent } from '@universis/ngx-qa';
import { AvailableServiceGuard } from './available-service.guard';

export const routes: Routes = [
  {
    path: 'lang/:id/:route',
    component: LangComponent
  },
  {
    path: 'lang/:id/:route/:subroute',
    component: LangComponent,
    pathMatch: 'prefix'
  },
  {
    path: 'error/498',
    redirectTo: 'auth/loginAs'
  },
  {
    path: 'error/401.1',
    redirectTo: 'auth/loginAs'
  },
  {
    path: 'error/0',
    redirectTo: 'error/408.1'
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        //loadChildren: './dashboard/dashboard.module#DashboardModule'
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'courses',
        //loadChildren: './courses/courses.module#CoursesModule'
        loadChildren: () => import('./courses/courses.module').then(m => m.CoursesModule)
      },
      {
        path: 'theses',
        //loadChildren: './theses/theses.module#ThesesModule'
        loadChildren: () => import('./theses/theses.module').then(m => m.ThesesModule)
      },
      {
        path: 'students',
        //loadChildren: './students/students.module#StudentsModule'
        loadChildren: () => import('./students/students.module').then(m => m.StudentsModule)
      },
      {
        path: 'profile',
        //loadChildren: './profile/profile.module#ProfileModule'
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'info',
        //loadChildren: './info-pages/info-pages.module#InfoPagesModule'
        loadChildren: () => import('./info-pages/info-pages.module').then(m => m.InfoPagesModule)
      },
      {
        path: 'messages',
        //loadChildren: './messages/messages.module#MessagesModule'
        loadChildren: () => import('./messages/messages.module').then(m => m.MessagesModule)
      },
      {
        path: 'outline',
        component: InstructorOutlineTabsComponent,
        canActivate: [
          AvailableServiceGuard
        ],
        data : {
          serviceDependencies: [
            'QualityAssuranceService'
          ]
        },
        children: [
          {
            path: '',
            redirectTo: 'general',
            pathMatch: 'full'
          },
          {
            path: 'general',
            component: InstructorOutlineGeneralComponent
          },
          {
            path: 'publications',
            component: InstructorOutlinePublicationsComponent
          }
        ]
      },
      {
        path: 'consultedStudents',
        //loadChildren: './consulted-students/consulted-students.module#ConsultedStudentsModule'
        loadChildren: () => import('./consulted-students/consulted-students.module').then(m => m.ConsultedStudentsModule)
      },
      {
        path: 'events',
        loadChildren: () => import('./instructor-events/instructor-events.module').then(m => m.InstructorEventsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // enableTracing: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
