import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditThesisProposalComponent } from './edit-thesis-proposal.component';

describe('EditThesisProposalComponent', () => {
  let component: EditThesisProposalComponent;
  let fixture: ComponentFixture<EditThesisProposalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditThesisProposalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditThesisProposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
