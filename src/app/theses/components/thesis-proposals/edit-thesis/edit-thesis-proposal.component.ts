import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';
import * as Quill from 'quill';

@Component({
  selector: 'app-edit-thesis-proposal',
  templateUrl: './edit-thesis-proposal.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
    .modal-dialog {
        position: relative;
        max-width: 80%;
    }

    .inner__content {
        max-height: 70vh;
        overflow-y: auto;
    }
    @media (max-width: 1200px) {
        .inner__content {
            max-height: unset;
            overflow: hidden;
        }
      
        .modal-dialog {
            position: relative;
            max-width: 100%;
        }
    }
    .ql-toolbar.ql-snow {
      border: 1px solid #E4E7EA;
    }
    .ql-toolbar {
      border-top-left-radius: .375rem !important;
      border-top-right-radius: .375rem !important;
    }
    .ql-container.ql-snow {
      border: 1px solid #E4E7EA;
    }
    .ql-container {
      font-family: inherit;
      line-height: 1.75;
      min-height: 192px;
      font-size: .875rem;
      border-bottom-left-radius: .375rem !important;
      border-bottom-right-radius: .375rem !important;
    }
    `
  ]
})

export class EditThesisProposalComponent extends RouterModalOkCancel implements OnInit, OnDestroy, AfterViewInit {

  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  dataSubscription: Subscription;
  paramSubscription: Subscription;
  @ViewChild('form') form?: AdvancedFormComponent;
  @ViewChild('notes', { static: true }) notes: ElementRef;
  public formConfig: string;
  public model: any;
  public thesisID: any;
  private notesEditor: any;
  private QuillEditor: any = Quill;
  public isLoading: boolean = true;

  constructor(protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translate: TranslateService,
    private _loading: LoadingService,
    private _toastService: ToastService,
    private _errorService: ErrorService) {
    super(_router, _activatedRoute);
    this.modalTitle = this._translate.instant('iTheses.ThesisProposalTitle');
  }

  ngOnInit(): void {
    this._loading.showLoading();
    this.isLoading = true;
    this.paramSubscription = this._activatedRoute.params.subscribe(async params => {
      if (params && params.hasOwnProperty('id')) {
        this.thesisID = params['id'];
        await this._context.model('Instructors/Me/thesisProposals')
          .where('id')
          .equal(this.thesisID)
          .expand('type,department,studyLevel,status,subject')
          .getItem().then(res => {
              this.model = res;
              this.formConfig = this.model.status && this.model.status.alternateName === 'potential' ? 'Instructors/Me/Thesis/editPotential' : 'Instructors/Me/Thesis/editCompleted';
          }).catch(err => {
            console.log(err);
            return this._errorService.navigateToError(err);
          });
      } else {
        this.model = {};
        this.formConfig = 'Instructors/Me/Thesis/new';
      }
      this._loading.hideLoading();
      this.isLoading = false;
    });
  }

  ngAfterViewInit() {
    const QuillEditor: any = Quill;
    this.notesEditor = new QuillEditor(this.notes.nativeElement, {
      theme: 'snow'
    });
    if (this.model.notes) {
      this.notesEditor.root.innerHTML = this.model.notes;
    }
    this.notesEditor.on('text-change', (event) => {
      this.model.notes = this.notesEditor.root.innerHTML;
      this.modelChange.emit(this.model);
    });
  }


  ok(): Promise<any> {
    let thesis: any = {};
    if (this.form.form.formio.data) {
      thesis = this.form.form.formio.data;
      thesis.status = thesis.status ? thesis.status : {alternateName: 'potential'};
    }
    return this._context.model(`instructors/me/createThesisProposal`).save(thesis)
      .then(() => {
        // show success toast message
        this._toastService.show(
          this._translate.instant('iTheses.ThesisProposals.EditThesis.SuccessTitle'),
          this._translate.instant('iTheses.ThesisProposals.EditThesis.SuccessMessage')
        );
        // close modal
        this.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      }).catch(err => {
        // show fail toast message
        this._toastService.show(
          this._translate.instant('iTheses.ThesisProposals.EditThesis.FailTitle'),
          this._translate.instant('iTheses.ThesisProposals.EditThesis.FailMessage')
        );
        // close modal without triggering a reload
        this.cancel();
      });
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }

  }
}


