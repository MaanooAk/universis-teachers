import { DatePipe } from '@angular/common';
import { Component, Injector, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ToastService, LoadingService, ModalService, ErrorService, ConfigurationService, AppEventService } from '@universis/common';
import { cloneDeep, template } from 'lodash';
import { Subscription, combineLatest } from 'rxjs';
import { ThesesService } from '../../services/theses.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { ProfileService } from 'src/app/profile/services/profile.service';

@Component({
  selector: 'app-thesis-requests-table',
  templateUrl: './thesis-requests-table.component.html',
  styleUrls: ['./thesis-requests-table.component.scss']
})
export class ThesisRequestsTableComponent implements OnInit, OnDestroy {
  private _paramSubscription: Subscription;
  public thesisProposal: any;
  public recordsTotal: number;
  public formattedDate: any = {};
  public showButtons = false;
  public isLoading = true;
  public back = true;
  public modalRef!: BsModalRef;
  public student: any;
  private thesisID: any;
  public disableButtons: boolean = false;
  public count: number;
  public totalStudentAccept: number;
  private instructorUser: number;
  public thesisRequestActions: any;

  constructor(
    private _loadingService: LoadingService,
    private _modalService: BsModalService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _activatedRoute: ActivatedRoute,
    private _thesesService: ThesesService,
    private _loading: LoadingService, 
    private _profileService: ProfileService
  ) { }


  async ngOnInit() {
    this.instructorUser = (await this._profileService.getInstructor()).user;
    this._paramSubscription = this._activatedRoute.params.subscribe(async (params) => {
      this.showButtons = true;
      if (params && params['id']) {
        this._loadingService.showLoading();
        this.isLoading = true;
        this.thesisID = params['id'];
        try {
          const thesisProposal = await this._context.model('instructors/me/thesisProposals')
            .where('id ')
            .equal(params['id'])
            .expand('status')
            .getItem();
          if (thesisProposal) {
            this.thesisRequestActions = thesisProposal.students;
            if (this.thesisRequestActions) {
              this.thesisProposal = await this.matchActions(thesisProposal, this.thesisRequestActions);
            }
          }
          this.count = this.thesisProposal && this.thesisProposal.students && this.thesisProposal.students.length;
          this.showButtons = true;
          this.isLoading = false;
          this._loadingService.hideLoading();
        } catch (err) {
          console.log(err);
          this.isLoading = false;
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        }
      }
    });
  }


  async matchActions(proposal: any, requests: any) {
    // Deep clone proposal to match 
    let fixedProposal = cloneDeep(proposal);

    for (let student of fixedProposal.students) {
        Object.assign(student, {
            STUDENT_INTERESTED: false,
            INSTRUCTOR_ACCEPTED: false,
            INSTRUCTOR_REJECTED: false,
            STUDENT_ACCEPTED: false,
            STUDENT_REJECTED: false,
            REQUEST_REJECTED: false,
            request: student
        });

        for (let action of student.actions) {
            const act = fixedProposal.actions.find(item => item.id === action.id);
            if (act) {
                Object.assign(action, act);
            }
        }

        student.STUDENT_INTERESTED = student.actions.some((el: { actionStatus: { alternateName: string; }; owner: number; }) =>
            el.actionStatus && ['ActiveActionStatus', 'CompletedActionStatus'].includes(el.actionStatus.alternateName) && el.owner === this.instructorUser
        );
        student.INSTRUCTOR_ACCEPTED = student.actions.some((el: { actionStatus: { alternateName: string; }; owner: number; }) =>
            el.actionStatus && el.actionStatus.alternateName === 'CompletedActionStatus' && el.owner === this.instructorUser
        );
        student.INSTRUCTOR_REJECTED = student.actions.some((el: { actionStatus: { alternateName: string; }; owner: number; modifiedBy: number; }) =>
          el.actionStatus && el.actionStatus.alternateName === 'CancelledActionStatus' && el.owner === this.instructorUser && el.modifiedBy === this.instructorUser
        );
        student.STUDENT_ACCEPTED = student.actions.some((el: { actionStatus: { alternateName: string; }; owner: number; }) =>
            el.actionStatus && el.actionStatus.alternateName === 'CompletedActionStatus' && el.owner !== this.instructorUser
        );
        student.STUDENT_REJECTED = student.actions.some((el: { actionStatus: { alternateName: string; }; owner: number; modifiedBy: number;}) =>
          el.actionStatus && el.actionStatus.alternateName === 'CancelledActionStatus' && (el.owner !== this.instructorUser ||  (el.owner === this.instructorUser && el.modifiedBy !== this.instructorUser) )
        );
        student.REQUEST_REJECTED = student.request && student.request.actionStatus === 'CancelledActionStatus';
    }

    // Order by dateCreated
    fixedProposal.students.forEach(student => {
        student.earliestDateCreated = student.actions.reduce((earliest, action) => {
            const actionDate = new Date(action.dateCreated);
            return actionDate < earliest ? actionDate : earliest;
        }, new Date());
    });

    fixedProposal.students.sort((a, b) => a.earliestDateCreated - b.earliestDateCreated);
    console.log("🚀 ~ ThesisRequestsTableComponent ~ matchActions ~ fixedProposal:", fixedProposal);
    return fixedProposal;
}


  ngOnDestroy() {
    if (this._paramSubscription && !this._paramSubscription.closed) {
      this._paramSubscription.unsubscribe();
    }
  }

  async toggleMore(i: number, studentID: number) {
    if (this.thesisProposal.students[i].show && this.thesisProposal.students[i].show === true) {
      this.thesisProposal.students[i].show = !this.thesisProposal.students[i].show;
    } else {
      this._thesesService.getStudentNotes(this.thesisProposal.id).then(async (res: any) => {
        const studentProposal = res;
        if (studentProposal) {
          const studentRequest = studentProposal.students.filter((el) => el.id === studentID ? el : null)[0];
          Object.assign(this.thesisProposal.students[i],
            { 'show': true, 'notes': studentRequest.notes });
        }
        this._loading.hideLoading();
        this.isLoading = false;
      }).catch(err => {
        return this._errorService.navigateToError(err);
      });
    }
  }

  acceptStudentModal(templateToLoad: TemplateRef<any>, student: any) {
    this.student = student;
    this.totalStudentAccept = this.thesisProposal.students.filter(el => el.STUDENT_ACCEPTED).length;
    this.modalRef = this._modalService.show(templateToLoad);
  }


 rejectStudentModal(templateToLoad: TemplateRef<any>, student: any) {
    this.student = student;
    this.modalRef = this._modalService.show(templateToLoad);
  }


  submit(data: any) {
    this._loading.showLoading();
    this.modalRef.hide();
    const body = {
      "actionStatus": {
        "alternateName": data
      }
    }
    return this._context.model(`instructors/me/thesisProposals/${this.thesisProposal.id}/actions/${this.student.actions[0].id}`).save(body)
      .then(async (res) => { 
        return await this._context.model('instructors/me/thesisProposals')
          .where('id ')
          .equal(this.thesisID)
          .expand('status')
          .getItem().then(async (res) => {
            this.thesisProposal = await this.matchActions(res, this.thesisRequestActions);
            this.count = this.thesisProposal && this.thesisProposal.students && this.thesisProposal.students.length;
            this._loading.hideLoading();
            this.isLoading = false;
          }).catch((err: any) => { 
            console.log(err);
            this._loading.hideLoading();
            this.isLoading = false;
            return this._errorService.navigateToError(err)
          });
      }).catch((err: any) => {
        console.log(err);
        this._loading.hideLoading();
        this.isLoading = false;
        this._errorService.navigateToError(err)
      });
  }

}
