import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';

import { ThesesRoutingModule } from './theses-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { ThesesCompletedComponent } from './components/theses-completed/theses-completed.component';
import { ThesesCurrentsComponent } from './components/theses-currents/theses-currents.component';

import {NgPipesModule} from 'ngx-pipes';
import {FormsModule} from '@angular/forms';
import {LoadingService, ModalService, SharedModule} from '@universis/common';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import { TeachersSharedModule } from '../teachers-shared/teachers-shared.module';

// LOCALES: import extra locales here
import * as el from './i18n/theses.el.json';
import * as en from './i18n/theses.en.json';
import { ThesesAdvancedHomeComponent } from './components/theses-advanced-home/theses-advanced-home.component';
import { ThesesAdvancedTableComponent } from './components/theses-advanced-table/theses-advanced-table.component';
import { ThesesAdvancedDefaultTableConfigurationResolver, ThesesAdvancedTableConfigurationResolver, ThesesAdvancedTableSearchResolver } from './components/theses-advanced-table/theses-advanced-table-config.resolver';
import { TablesModule } from '@universis/ngx-tables';
import { AdvancedFormItemResolver, AdvancedFormResolver, AdvancedFormsModule, AdvancedFormsService } from '@universis/forms';
import { RouterModalModule } from '@universis/common/routing';
import { RouterModule } from '@angular/router';
import { ModalDetailsComponent } from './components/modal-details/modal-details.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { EditThesisProposalComponent } from './components/thesis-proposals/edit-thesis/edit-thesis-proposal.component';
import { ThesisProposalOverviewComponent } from './components/thesis-proposals/thesis-proposal-overview/thesis-proposal-overview.component';
import { ThesisRequestsTableComponent } from './components/thesis-requests-table/thesis-requests-table.component';
import { ThesisProposalsComponent } from './components/thesis-proposals/thesis-proposals.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ThesesRoutingModule,
    TranslateModule,
    BsDropdownModule,
    NgPipesModule,
    FormsModule,
    TablesModule,
    TeachersSharedModule, 
    AdvancedFormsModule, 
    RouterModalModule,
    RouterModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    ThesesAdvancedTableConfigurationResolver,
    ThesesAdvancedDefaultTableConfigurationResolver,
    ThesesAdvancedTableSearchResolver,
    ModalService,
    BsModalService,
    LoadingService,
    AdvancedFormsService,
    AdvancedFormResolver
  ],
  declarations: [ThesesCompletedComponent, ThesesCurrentsComponent, ThesesAdvancedHomeComponent, ThesesAdvancedTableComponent, ModalDetailsComponent,
    ThesisProposalsComponent,
    EditThesisProposalComponent,
    ThesisProposalOverviewComponent,
    ThesisRequestsTableComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})

export class ThesesModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
