import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MessagesListComponent} from './components/messages-list/messages-list.component';
import {MessagesHomeComponent} from './components/messages-home/messages-home.component';
import {AuthGuard} from '@universis/common';

const routes: Routes = [
  {
    path: '',
    component: MessagesHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: MessagesListComponent
      },
      {
        path: 'list/:selectedMessages',
        component: MessagesListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRouting {
}
