import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import { TranslateModule} from '@ngx-translate/core';
import { MessagesService} from '../../services/messages.service';
import {
  ConfigurationService,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService
} from '@universis/common';
import { SendMessageToStudentComponent } from './send-message-to-student.component';
import {RouterTestingModule} from '@angular/router/testing';
import {BsModalService, ModalModule} from 'ngx-bootstrap/modal';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingConfigurationService} from '@universis/common/testing';
import {MostModule} from '@themost/angular';
import {MessageSharedService} from '../../../teachers-shared/services/messages.service';

describe('SendMessageToStudentComponent', () => {
  let component: SendMessageToStudentComponent;
  let fixture: ComponentFixture<SendMessageToStudentComponent>;
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ModalModule.forRoot(),
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false,
            useResponseConversion: true
          }
        })
      ],
      declarations: [ SendMessageToStudentComponent ],
      providers: [
        BsModalService,
        ToastService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        MessagesService,
        MessageSharedService,
        {
          provide: LoadingService,
          useValue: loadingSvc
        },
        ModalService,
        ErrorService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToStudentComponent);
    component = fixture.componentInstance;
    component.courseClassId = Math.round(Math.random() * 1000);
    component.studentId = Math.round(Math.random() * 1000);
    component.messageModel = {
      body: 'Random',
      subject: 'MessageToStudent'
    };
    fixture.detectChanges();
  });

  it('should create', inject([MessagesService], (_msgSvc: MessagesService) => {
    spyOn(_msgSvc, 'getAllMessages').and.callFake(() => {
      return Promise.resolve(JSON.parse('{"value": []}'));
    });
    expect(component).toBeTruthy();
  }));
});
