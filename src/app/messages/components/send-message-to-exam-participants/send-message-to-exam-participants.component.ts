import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { LoadingService, ModalService, ToastService, ErrorService } from '@universis/common';
import { MessagesService } from '../../services/messages.service';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-send-message-to-exam-participants',
  templateUrl: './send-message-to-exam-participants.component.html'
})
export class SendMessageToExamParticipantsComponent implements OnInit {

  @Output() successfulSend = new EventEmitter<boolean>();
  @ViewChild('fileInput') fileInput;

  public instructor: any;
  public courseExam: any;
  public messageModel = {
    body: null,
    file: null,
    subject: null
  };

  constructor(
    private _loadingService: LoadingService,
    private _messagesService: MessagesService,
    private _toastService: ToastService,
    private _translate: TranslateService,
    public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

  initData() {
    this.messageModel.file = null;
    this.messageModel.body = null;
    this.messageModel.subject = null;
  }

  closeModal() {
    this.bsModalRef.hide();
  }

  /**
   *
   * Triggers an email send to current exam participants
   *
   */
  async sendMessage(): Promise<any> {
    this._loadingService.showLoading();
    try {
      await this._messagesService.sendMessageToExamParticipants(this.courseExam.id, this.messageModel);
      this.messageSent(true);
      this.showToastNotification(true);
    } catch (err) {
      console.error(err);
      this.messageSent(false);
      this.showToastNotification(false);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * Shows a toast notification to the user
   *
   * @param successful States whether the email was sent successfully
   *
   */
  showToastNotification(successful: boolean): void {
    const container = document.body.getElementsByClassName('universis-toast-container')[0];
    if (successful) {
      if (container != null) {
        container.classList.add('toast-success');
      }
      this._toastService.show( 
        this._translate.instant('Modals.SuccessfulSendMessageTitle'),
        this._translate.instant('Messages.MessageNotifications.RegisteredEmail')
      );
    } else {
      if (container != null) {
        container.classList.add('toast-error');
      }
      this._toastService.show(
        this._translate.instant('Modals.FailedSendMessageTitle'),
        this._translate.instant('Modals.FailedSendMessageMessage')
      );
    }
  }

  onFileChanged(event) {
    this.messageModel.file = event.target.files[0];
  }

  reset(event) {
    event.target.value = '';
    this.messageModel.file = null;
  }

  messageSent(successful: boolean) {
    this.successfulSend.emit(successful);
  }
}
