import { Component, OnInit, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MessagesService } from '../../services/messages.service';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService, ConfigurationService } from '@universis/common';
import { BsModalRef } from 'ngx-bootstrap/modal';



@Component({
  selector: 'app-send-message-to-class',
  templateUrl: './send-message-to-class.component.html'
})
export class SendMessageToClassComponent implements OnInit {
  public defaultLanguage: string | any = "";
  public currentLanguage = "";

  public messageModel = {
    body: null,
    file: null,
    subject: null
  };
  @Output() succesfulSend = new EventEmitter<boolean>();
  @ViewChild('fileinput') fileinput;
  showMessageForm: any;
  instructor: any;
  courseClass: any;

  constructor(private _errorService: ErrorService,
    private _loadingService: LoadingService,
    private _messagesService: MessagesService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _translate: TranslateService,
    public bsModalRef: BsModalRef,
    private _configurationService: ConfigurationService
  ) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService.settings?.i18n?.defaultLocale;
  }

  ngOnInit() {
  }
  initData() {
    this.messageModel.file = null;
    this.messageModel.body = null;
    this.messageModel.subject = null;
  }
  closeModal() {
    this.ngOnInit();
    this.bsModalRef.hide();
  }
  sendClassMessage() {
    this._loadingService.showLoading();
    this._messagesService.sendMessageToClass(this.courseClass.id, this.messageModel)
      .subscribe(msgSend => {
        // complete the action
        this.initData();
        this._loadingService.hideLoading();
        this._toastService.show(this._translate.instant('Modals.SuccessfulSendMessageTitle'),
          this._translate.instant('Messages.MessageNotifications.RegisteredEmail'));
        const container = document.body.getElementsByClassName('universis-toast-container')[0];
        if (container != null) {
          container.classList.add('toast-success');
        }
        this.messageSent(true);
      }, (err) => {
        this._loadingService.hideLoading();
        this._toastService.show(this._translate.instant('Modals.FailedSendMessageTitle'),
          this._translate.instant('Modals.FailedSendMessageMessage'));
        const container = document.body.getElementsByClassName('universis-toast-container')[0];
        if (container != null) {
          container.classList.add('toast-error');
        }
        this.messageSent(false);
      });
  }
  onFileChanged(event) {
    this.messageModel.file = event.target.files[0];
  }
  reset(event) {
    event.target.value = '';
    this.messageModel.file = null;
  }
  messageSent(succesful: boolean) {
    this.succesfulSend.emit(succesful);
  }

}
