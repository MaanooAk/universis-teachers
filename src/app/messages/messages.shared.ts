import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SendMessageToStudentComponent } from './components/send-message-to-student/send-message-to-student.component';
import { SendMessageToClassComponent } from "./components/send-message-to-class/send-message-to-class.component";
import { SendMessageToExamParticipantsComponent } from "./components/send-message-to-exam-participants/send-message-to-exam-participants.component";
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';

// LOCALES: import extra locales here
import * as el from './i18n/messages.el.json';
import * as en from './i18n/messages.en.json';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    NgxDropzoneModule,
    SharedModule
  ],
  declarations: [
    SendMessageToClassComponent,
    SendMessageToExamParticipantsComponent,
    SendMessageToStudentComponent,
    ComposeMessageComponent
  ],
  exports: [
    SendMessageToStudentComponent,
    SendMessageToClassComponent,
    SendMessageToExamParticipantsComponent,
    ComposeMessageComponent
  ],
  providers: [

  ],
  entryComponents: [
    SendMessageToExamParticipantsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessagesSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading messages shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }

}
