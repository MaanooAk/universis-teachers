import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class ClassesService {

  constructor(private _context: AngularDataContext) { }

  getAllInstructors(): any {
    return this._context.model('instructors/me')
      .asQueryable()
      .expand('department($expand=locale)')
      .getItem();
  }

  getAllClasses(): any {
  return this._context.model('instructors/me/classes')
    .asQueryable()
    .expand('year,period,course($expand=department($expand=locale),locale)')
    .orderByDescending('year')
    .thenByDescending('period')
    .getList();
  }

  getCurrentClasses(): any {
    return this._context.model('instructors/me/currentClasses')
      .asQueryable()
      .expand('period($expand=name),year($expand=alternateName)')
      .getList();
  }
}