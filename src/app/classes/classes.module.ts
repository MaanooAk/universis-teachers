import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {SharedModule} from '@universis/common';
import { ClassesRoutingModule } from './classes-routing.module';
import { ClassesAllComponent } from './components/classes-all/classes-all.component';
import { NgPipesModule} from 'ngx-pipes';

// LOCALES: import extra locales here
import * as el from './i18n/classes.el.json';
import * as en from './i18n/classes.en.json';

@NgModule({
  imports: [
    CommonModule,
    ClassesRoutingModule,
    TranslateModule,
    NgPipesModule,
    SharedModule
  ],
  declarations: [ClassesAllComponent]
})
export class ClassesModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
