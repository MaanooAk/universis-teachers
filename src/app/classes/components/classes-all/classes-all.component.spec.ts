import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesAllComponent } from './classes-all.component';
import { ClassesService } from '../../services/classes.service';
import { FormsModule } from '@angular/forms';
import { AppSidebarModule } from '@coreui/angular';
import { CommonModule, KeyValuePipe } from '@angular/common';
import { NgArrayPipesModule } from 'angular-pipes';
import { NgPipesModule } from 'ngx-pipes';
import { RouterTestingModule } from '@angular/router/testing';
import { MostModule } from '@themost/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule,ConfigurationService } from '@universis/common';

describe('ClassesAllComponent', () => {
  let component: ClassesAllComponent;
  let fixture: ComponentFixture<ClassesAllComponent>;

  const classesSvc = jasmine.createSpyObj('ClassesService,', ['getAllInstructors', 'getAllClasses', 'getCurrentClasses']);
  let _configurationService: ConfigurationService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgPipesModule,
        RouterTestingModule,
        ConfigurationService,
        SharedModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
              useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [ ClassesAllComponent ],
      providers: [
        {
          provide: ClassesService,
          useValue: classesSvc
        }
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
