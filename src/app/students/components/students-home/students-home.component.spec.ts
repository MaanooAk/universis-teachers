import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsHomeComponent } from './students-home.component';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { StudentsService } from '../../services/students.service';
import { ErrorService } from '@universis/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@universis/common';

describe('StudentsHomeComponent', () => {
  let component: StudentsHomeComponent;
  let fixture: ComponentFixture<StudentsHomeComponent>;

  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);
  const studentsSvc = jasmine.createSpyObj('StudentsService,', ['getStudentBySearch', 'getThesesBySearch']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
              useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [ StudentsHomeComponent ],
      providers: [
        {
          provide: StudentsService,
          useValue: studentsSvc
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
