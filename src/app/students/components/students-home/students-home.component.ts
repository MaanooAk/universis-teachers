import { Component, OnInit } from '@angular/core';
import {StudentsService} from '../../services/students.service';
//import {forEach} from '@angular/router/src/utils/collection';
import {ErrorService, ConfigurationService} from '@universis/common';


@Component({
  selector: 'app-students-home',
  templateUrl: './students-home.component.html',
  styleUrls: ['./students-home.component.scss']
})
export class StudentsHomeComponent implements OnInit {
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";

  public studentResults; // Data
  public isLoading = true;        // Only if data is loaded
  public searchText = '';
  public searchStarted = false;
  public searchFinished = true;
  public index_arr: any;
  public searchTheses: any = [];
  public searchStudents: any = [];
  public student: any;
  public students: any = [];
  public isCollapsed?: boolean[];


  constructor(private studentsService: StudentsService,
              private  errorService: ErrorService,
              private _configurationService: ConfigurationService) {
                this.currentLanguage = this._configurationService.currentLocale;
                this.defaultLanguage = this._configurationService.settings?.i18n?.defaultLocale;
  
  }


  ngOnInit() {
    this.isLoading = false;
  }

  getData(searchText: any) {
    this.isLoading = true;
    this.searchStarted = true;
    this.searchFinished = false;
    this.studentsService.getStudentBySearch(searchText).then((res) => {
      this.searchStudents = res;
      this.studentsService.getThesesBySearch(searchText).then((res2) => {
        this.searchTheses = res2;
        this.isLoading = false;
        this.studentResults = this.searchStudents.concat(this.searchTheses);
        this.searchFinished = true;
        this.index_arr = removeDuplicates(this.studentResults);
        this.isCollapsed = Array(this.index_arr.length).fill(true);
        // loop through array of unique student ids
        this.students.length = 0;
        for (let i = 0; i < this.index_arr.length; i++) {
          // first find student
          this.student = this.studentResults.find(x =>
            x.student.id === this.index_arr[i]).student;
          // filter classes for this student and add to student.classes.collection
          this.student.classes = this.searchStudents.filter((x) => {
            return x.student.id === this.student.id && x.formattedGrade !== null;
          }).sort((a, b) => {
            return  a.courseClass.title < b.courseClass.title   ? -1  : 1 ;
          });
          // filter theses for this student and add to student.theses.collection
          this.student.theses = this.searchTheses.filter((x) => {
            return x.student.id === this.student.id;
          });
          // add student to array of students
          this.students.push(this.student);
        }
      }).catch(err => {
        this.isLoading = false;
        return this.errorService.navigateToError(err);
      });
    }).catch(err => {
      this.isLoading = false;
      return this.errorService.navigateToError(err);
    });
  }
}

function removeDuplicates(arr: any) {
  const unique_array:any[] = [];
  for (let i = 0; i < arr.length; i++) {
    if (unique_array.indexOf(arr[i].student.id) === -1) {
      unique_array.push(arr[i].student.id);
    }
  }
  return unique_array;
}


