import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AppSidebarService } from './services/app-sidebar.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
//import { NgSpinKitModule } from 'ng-spin-kit';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RouterModule } from '@angular/router';
import { LangComponent } from './lang-switch.component';
import { environment } from '../../environments/environment';
import { ApplicationSettingsConfiguration, SharedModule } from '@universis/common';

// LOCALES: import extra locales here
import * as el from '../../assets/i18n/el.json';
import * as en from '../../assets/i18n/en.json';


export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  contactUrl: any;
  useDigitalSignature: boolean;
  navigationLinks?: any;
  websocket: string;
  checkTokenInfo: boolean;
  title?: string;
  header?: Array<any>;
  maxExportItems?: number;
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    BsDropdownModule,
    ModalModule,
    //NgSpinKitModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    LangComponent
  ],
  exports: [
    LangComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class TeachersSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: TeachersSharedModule,
    private _translateService: TranslateService) {
    if (parentModule) {
      // throw new Error(
      //    'TeachersSharedModule is already loaded. Import it in the AppModule only');
    }
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading students shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: TeachersSharedModule,
      providers: [
        AppSidebarService
      ]
    };
  }

  async ngOnInit() {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
}
