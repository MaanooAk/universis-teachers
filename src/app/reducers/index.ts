import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromSettings from '../settings/reducers';
export interface AppState {
  settings: fromSettings.SettingsState;
}

export const reducers: ActionReducerMap<AppState> = {
  settings: (fromSettings.filterReducer as any)
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
