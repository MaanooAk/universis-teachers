import { TestBed } from '@angular/core/testing';

import { CoursesService } from './courses.service';
import { ConfigurationService } from '@universis/common';
import { TestingConfigurationService } from 'src/app/test';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';

describe('CoursesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      SharedModule,
      MostModule.forRoot({
        base: '/',
        options: {
            useMediaTypeExtensions: false
        }
      })
    ],
    providers:[
      {
        provide: ConfigurationService,
        useClass: TestingConfigurationService
      }
    ]
  }));

  it('should be created', () => {
    const service: CoursesService = TestBed.get(CoursesService);
    expect(service).toBeTruthy();
  });
});
