/**
 *
 * Courses details grading index component
 *
 * Is a wrapper for the cousrse details grading components for routing purposes
 *
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import { ErrorService } from '@universis/common';
import { Subscription} from "rxjs";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-courses-details-grading-index',
  template: ``,
})
export class CoursesDetailsGradingIndexComponent implements OnInit, OnDestroy {

  private routeParamsSubscription?: Subscription;

  constructor(private coursesService: CoursesService,
    private errorService: ErrorService,
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnDestroy(): void {
    if (this.routeParamsSubscription) {
      this.routeParamsSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.routeParamsSubscription = this.route.parent?.parent?.params.subscribe(routeParams => {
      return this.coursesService
        .getCourseClass(
          routeParams['course'],
          routeParams['year'],
          routeParams['period']
        ).then(courseClass => {
          if (courseClass) {
            return this.coursesService
              .getCourseClassExams(courseClass.id)
              .then(courseExams => {
                if (courseExams && courseExams.length) {
                   return this.router.navigate([courseExams[0].id], { relativeTo: this.route });
                }
              });
          }
        }).catch(err => {
          return this.errorService.navigateToError(err);
        });
    });
  }
}
