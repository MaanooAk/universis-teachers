import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-course-details-exam-switch',
  templateUrl: './course-details-exam-switch.component.html',
  styleUrls: ['./course-details-exam-switch.component.scss']
})
export class CourseDetailsExamSwitchComponent implements OnInit {
  @Input() studentsView: boolean | any;
  public routerLinkGeneral;
  public routerLinkStudents;

  constructor() { }

  ngOnInit() {
    this.routerLinkGeneral = this.studentsView ? ['../'] : ['.'];
    this.routerLinkStudents = this.studentsView ? ['.'] : ['students'];
  }

}
