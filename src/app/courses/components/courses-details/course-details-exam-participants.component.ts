/**
 *
 * Course details exam component
 *
 * Contains information about a specific exam.
 * Exam information is being fetched from the route.
 *
 */

import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {CoursesService} from '../../services/courses.service';
import {BehaviorSubject} from 'rxjs';


@Component({
  selector: 'app-courses-details-exam-participants',
  templateUrl: './course-details-exam-participants.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsExamParticipantsComponent implements OnInit, OnDestroy {

  public isLoading = true;
  private _courseExam: any;
  @Input()
  get courseExam(): any {
    return this._courseExam;
  }
  set courseExam(value: any) {
    this._courseExam = value;
    this._courseExamSubscription.next(this._courseExam);
  }
  private _courseExamSubscription = new BehaviorSubject<any>(null);
  public participants: any;

  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private  coursesService: CoursesService
  ) {
  }

  async ngOnInit() {
    this._courseExamSubscription.subscribe( result => {
      if (result) {
        this.coursesService.getExamParticipants(result.id).then( participants => {
          this.participants = participants;
          this.isLoading = false;
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this._courseExamSubscription) {
      this._courseExamSubscription.unsubscribe();
    }
  }

  exportParticipants(isCsv) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    if (isCsv) {
      headers.set('Accept', 'text/csv');
    }
    const fileURL = this._context.getService().resolve(`instructors/me/exams/${this.courseExam.id}/participants/export`);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        const name = this.courseExam.name.replace(/[/\\?%*:|"<>]/g, '-');
        const extension = isCsv ? 'csv' : 'xlsx';
        const periodNumeric = (this.courseExam.examPeriod.id + '').padStart(3, "0")
        const downloadName = `${name}-${this.courseExam.year.id}-${periodNumeric}-${this.courseExam.examPeriod.name}-${this.translate.instant('Participation.Info')}.${extension}`
        a.download = downloadName;
        if (window.navigator && (window.navigator as any).msSaveOrOpenBlob) {
          (window.navigator as any).msSaveOrOpenBlob(blob, downloadName );
        } else {
          a.click();
        }
        window.URL.revokeObjectURL(objectUrl);
        a.remove(); // remove the element
      });
  }


}
